package serverRestSisDisPer;


import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.json.*;

@Path("/Server")
public class GameServer {
	
	static MatchesList matchlist= new MatchesList(); 
	
	@GET // Return the existing matches in the server
    @Produces(MediaType.APPLICATION_JSON) 
    public synchronized String getTheExistingMatches() throws JSONException {
		if(matchlist.matches.size()>0)
			return matchlist.getAllListInJson(0);
		else
			return matchlist.getAllListInJson(1);
    }
	
	@POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/AddMatch") // This will add a match inside the list -> Insert
    public String AddMatch(String jsonString) 
     {
		synchronized (matchlist){
			System.out.println("Entrato in post ");
	        MatchInfo match=null;
	        try{
	        	 match = new MatchInfo(jsonString);
	        }
	        catch(Exception e){
	        	System.out.println("Eccezione nel post in lettura");
	        	e.printStackTrace();
	        }

	        if(match!=null) // it the conversion is successfull
	        {
	        	int size=matchlist.matches.size();
	        	if(size>=1)
	        	{
	        		for (int i=0; i<size; i++)
	        		{
	            		if(matchlist.matches.get(i).namematch.equals(match.namematch)) // Check if there is already a match with the same name
	            			return MatchInfo.respondMessage(MatchInfo.MATCHEXISTS);
	            		else // just for now
	            			System.out.println("this element name: "+matchlist.matches.get(i).namematch+" and the one you sent: "+match.namematch);	
	            	}
	        	}	
	        	System.out.println(matchlist.matches.add(match));
	        		return MatchInfo.respondMessage(MatchInfo.OK);	
	    	}
		}
        return "Retry. Incorrect input";	// in case errors -> shouldn't occour
    }
	
	@PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/AddPlayer") // this will add a player in a match -> Update
    public String addAPlayer(String jsonString) throws JSONException{

		synchronized(matchlist){
			System.out.println("Entrato in put ");
	    	MatchInfo match=null;
	    	match = new MatchInfo(jsonString);

	       boolean ishere=false;
	       int index=-1;
	        // Find The match requested and add the player inside.
	       for(int i=0; i<matchlist.matches.size(); i++)
	       {
	    	   System.out.println("Size: "+matchlist.matches.size());
	    	   System.out.println("matchlist name: "+matchlist.matches.get(i).namematch);
	    	   System.out.println("This match name: "+match.namematch);
	    	   if(matchlist.matches.get(i).namematch.equals(match.namematch))
	    	   {
	    		   //System.out.println("It's in there");
	    		   ishere=true;
	    		   index=i;
	    		   break; 
	    	   }	   
	       }
	       if(ishere) // there is a match with the same name of the one requested.
	       {
	    	   for( int i =0; i< matchlist.matches.get(index).playerList.size(); i++)
	    	   {
	    		   if(matchlist.matches.get(index).playerList.get(i).username.equals(match.playerList.get(0).username))
	    			   return MatchInfo.respondMessage(MatchInfo.PLAYERINSIDE);   
	    	   }
	    	   
	    	   matchlist.matches.get(index).addPlayer(match.playerList.get(0).username, match.playerList.get(0).networkaddress,  match.playerList.get(0).port);
	    	   System.out.println(matchlist.matches.get(index).playerList.size());
	    	   System.out.println("Added a player");
	    	   
	    	   return matchlist.matches.get(index).getPlayers();
	       }
	       else
	    	   System.out.println("It's not in there");
		}
       return MatchInfo.respondMessage(MatchInfo.MATCHNOTFOUND); 
    }

    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/DeletePlayer") // this deletes a player, given the match info
    public  String deletePlayer(String jsonString) throws JSONException{
    	synchronized(matchlist){
    		System.out.println("Entrato in delete");
        	MatchInfo match=null;
        	match = new MatchInfo(jsonString);
        	for(int i=0; i<matchlist.matches.size(); i++)
            {
        		if(matchlist.matches.get(i).namematch.equals(match.namematch))
         	   	{
         		  System.out.println("It's in there");
         		  for( int index =0; index < matchlist.matches.get(i).playerList.size(); index++)
         		  {
         			  if(matchlist.matches.get(i).playerList.get(index).username.equals(match.playerList.get(0).username))
     				  {
         				 
         				 matchlist.matches.get(i).playerList.remove(index);
         				 if(matchlist.matches.get(i).playerList.size()<1)
         					 DeleteMatch(matchlist.matches.get(i));
         				 return MatchInfo.respondMessage(MatchInfo.OK);
     				  }
         		  }    		  
         		  return MatchInfo.respondMessage(MatchInfo.PLAYERNOTFOUND);
         	   	}	
            }
    	}
        return MatchInfo.respondMessage(MatchInfo.MATCHNOTFOUND);
    }
    
    // When there is no player inside, the server will close the match
    synchronized void DeleteMatch(MatchInfo match)
    {
    	System.out.println("A Match has been Deleted");
    	for(int i=0; i<matchlist.matches.size(); i++)
        {
    		if(matchlist.matches.get(i).namematch.equals(match.namematch))
     	   	{
     	   		matchlist.matches.remove(i);
     	   	}
        }
    }
    
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/DeleteMatch") // this deletes a match, asked from the player (not used)
    public synchronized String deleteMatch(String jsonString) throws JSONException{
    	
    	System.out.println("Entrato in delete");
    	MatchInfo match=null;
    	match = new MatchInfo(jsonString);
    	for(int i=0; i<matchlist.matches.size(); i++)
        {
    		if(matchlist.matches.get(i).namematch.equals(match.namematch))
     	   	{
    			matchlist.matches.remove(i);
    			return "Match deleted";
     	   	}
        }
    	return "Match not found";
    }

}
