package serverRestSisDisPer;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Class containing the match list
public class MatchesList {

	public final static int OK=0;
	public final static int MATCHNOTFOUND=1;
	
	List<MatchInfo> matches= new LinkedList<MatchInfo>();

	// this function should return all the matches in one shot
	public String getAllListInJson(int type) throws JSONException{
		JSONObject totmatches = new JSONObject();
		if(type==OK){
			JSONArray matcheslist = new JSONArray();
			for (MatchInfo m : this.matches) {
				
				JSONObject match = new JSONObject();
				 match.put("namematch", m.namematch);
		        match.put("lengthsidegrid", m.lengthsidegrid);
		        match.put("pointstowin", m.pointstowin);
				matcheslist.put(match);
			}
			
			totmatches.put("type", OK);
			totmatches.put("matches", matcheslist);
		}
		else{
			totmatches.put("type", MATCHNOTFOUND);
		}
		return totmatches.toString();
	}
	
	// This reads all the matches
	public MatchInfo[] fromJson(String jsonString) throws JSONException
	{
		JSONObject input = new JSONObject(jsonString);
		JSONArray array = input.getJSONArray("matches");	
		MatchInfo [] matches =new MatchInfo [array.length()];
		for (int i=0; i<array.length(); i++){
			JSONObject current = array.getJSONObject(i);
			matches[i]= new MatchInfo();
			matches[i].namematch= current.getString("namematch");
			matches[i].lengthsidegrid= current.getInt("lengthsidegrid");
			matches[i].pointstowin= current.getInt("pointstowin");
		}
		return matches;
	}
	
	// Response from the server. I need to know what he said.
	public static int typeOfMessage(String jsonMessage){
		
		try{
			JSONObject input = new JSONObject(jsonMessage);
			return input.getInt("type");
		}
		catch(Exception e){
			
		}
		return -1;
	}
		
}
