package serverRestSisDisPer;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the class with all the info about the match
 */
public class MatchInfo {
	// const vars for the type message
	public final static int OK=0;
	public final static int MATCHNOTFOUND=1;
	public final static int PLAYERINSIDE=2;
	public final static int MATCHEXISTS=3;
	public final static int PLAYERNOTFOUND=4;

	String namematch;
	int lengthsidegrid;
	int pointstowin;
	List<PlayerInfo> playerList= new ArrayList<PlayerInfo>();
	
	public String toJSONString() throws JSONException {
		//Add only one player
		
        JSONObject matchInfo = new JSONObject();
        	matchInfo.put("namematch", this.namematch);
            matchInfo.put("lengthsidegrid", this.lengthsidegrid);
            matchInfo.put("pointstowin", this.pointstowin);
            // to add the player
            if(playerList.size()==1){
            	JSONObject player = new JSONObject();
            	player.put("username", playerList.get(0).username);
            	player.put("networkaddress", playerList.get(0).networkaddress);
            	player.put("port", playerList.get(0).port);
            	matchInfo.put("player", player);
            }
            else{
            	JSONArray playerlist = new JSONArray();
                for (PlayerInfo p : this.playerList) {
        			
        			JSONObject player = new JSONObject();
        			 player.put("username", p.username);
        	        player.put("networkaddress", p.networkaddress);
        	        player.put("port", p.port);
        			playerlist.put(player);
        		}
                matchInfo.put("players", playerlist);
            }
        return matchInfo.toString();
    }
	
	// Create the message with the match from the jsonString
	public MatchInfo(String jsonString) throws JSONException {
		
        	JSONObject input = new JSONObject(jsonString);
        	  //System.out.println("after input");
            this.namematch=(input.getString("namematch"));
            this.lengthsidegrid= (input.getInt("lengthsidegrid"));
            this.pointstowin=(input.getInt("pointstowin"));
            // to add the players
            PlayerInfo player= new PlayerInfo(input.getString("player"));
            playerList.add(player);
    }
	
	 // Done for the client when it is needed.
	public MatchInfo() {
		// TODO Auto-generated constructor stub
	}
	
	//Made when i'll do it in the peer from IO
	public MatchInfo(String name,int length, int points){
		namematch=name;
		lengthsidegrid= length;
		pointstowin=points;	
	}

	public boolean addPlayer(String name, String networkaddress, int port) throws JSONException {
		
		PlayerInfo player= new PlayerInfo(name,networkaddress,port);
        return playerList.add(player);	
	}
	
	public String getPlayers() throws JSONException
	{
		JSONObject playerInfo = new JSONObject();
		JSONArray playerlist = new JSONArray();
        for (PlayerInfo p : this.playerList) {
			
			JSONObject player = new JSONObject();
			 player.put("username", p.username);
	        player.put("networkaddress", p.networkaddress);
	        player.put("port", p.port);
			playerlist.put(player);
		}
        playerInfo.put("type", OK);
        playerInfo.put("players", playerlist);

		return playerInfo.toString();
	}
	
	
	public static List<PlayerInfo> playersFromJson(String jsonString) throws JSONException{
		List<PlayerInfo> players = new ArrayList<PlayerInfo>();
		JSONObject input = new JSONObject(jsonString);
		JSONArray array = input.getJSONArray("players");
		for (int i=0; i<array.length(); i++){
			JSONObject current = array.getJSONObject(i);
			String username= current.getString("username");
			String networkaddress= current.getString("networkaddress");
			int port =current.getInt("port");
			PlayerInfo player=new PlayerInfo(username,networkaddress,port);
			players.add(player);	
		}
		return players;
	}
	
	public String getMatchName(){
		return namematch;
	}
	
	public int getLength(){
		return lengthsidegrid;
	}
	
	public int getPoints(){
		return pointstowin;
	}
	
	public static int typeOfMessage(String jsonMessage){
		int message=-1;
		try{
		JSONObject output= new JSONObject(jsonMessage);
		
			message=output.getInt("type");
		}
		catch(Exception e){
			System.out.println("Eccezione nel type");
		}
		return message;
	}
	
	public static String respondMessage(int type){	
		JSONObject output= new JSONObject();
		try{
			output.put("type", type);
		}
		catch(Exception e){
			System.out.println("Eccezione nel type");
		}
	
		return output.toString();
	}
}
