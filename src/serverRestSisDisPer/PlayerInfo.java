package serverRestSisDisPer;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;
// Class with the infos about the player
public class PlayerInfo {

	String username;
	String networkaddress; // da cambiare in stringa
	int port;
	
	public String toJSONString() throws JSONException {
	
		JSONObject playerinfo = new JSONObject();
		playerinfo.put("username", this.username);
		playerinfo.put("networkaddress", this.networkaddress);
		playerinfo.put("port", this.port);
		return playerinfo.toString();
	}
	
	public PlayerInfo(String jsonString) throws JSONException {

		 JSONObject input = new JSONObject(jsonString);
		 
		 this.username=(input.getString("username"));
		 this.networkaddress= (input.getString("networkaddress"));
		 this.port = (input.getInt("port")); 
	}
	
	public PlayerInfo(String name, String add, int port)
	{
		username=name;
		networkaddress=add;
		this.port=port;
	}
	
	public PlayerInfo(){
		
	}
	
	public void askStartingInfo() // This method will insert the initial info
	{
		BufferedReader input= new BufferedReader( new InputStreamReader(System.in));
		
		try{
			System.out.println("Salve! Scrivi il tuo username");
			this.username=input.readLine();
		}
		catch(Exception e){
			System.out.println("problema con input");
		}
		
		boolean correctinput=false;
		while(!correctinput){ // port control
			System.out.println("Scrivi la porta che vuoi usare");
			try{
				this.port= Integer.parseInt(input.readLine());
				if(port>4000)
					correctinput=true;
				else
					System.out.println("Errore! Usa una porta maggiore di 4000");
			}
			catch(Exception e){
				System.out.println("Errore! La porta deve essere un valore numerico");
			}
		}
		// the project didn't ask for address, so i'll give as assumption that i'm localhost
		this.networkaddress="localhost";
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getAddress()
	{
		return networkaddress;
	}
	
	public int getPort(){
		return port;
	}
}
