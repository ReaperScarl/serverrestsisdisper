package peerSisDisPer;
import serverRestSisDisPer.*;

import org.json.*;


// This is the class that has the job to know the type of message is arriving in
public class Message {

	final static int ENTER=0; // a player is entering the ring. There is a player add in the list
	final static int EAT=1; // a player has been eaten
	final static int BOMB=2; // a bomb message
	final static int TOKEN=3; // This message is a token
	final static int MOVE=4; // This message is to move
	final static int EXIT=5; // a player is exiting the ring after he's been killed
	final static int OK=6; // OK message need to say when the peer can continue
	final static int WIN=7;    // Someone is communicating that won
	final static int EXPLODED=8; // when the bomb explodes and i'm inside the area, i need to tell it.
	final static int FIRSTPOSITION=9; // a player is entering the game and is checking his position
	final static int ALREADYOCCUPIED=10; // a player is already in that position
	
	int type; // type of the message. can be only one of the said above
	// Variables depending on the message
	PlayerInfo player; 
	boolean bombIsExploded;
	Position position= new Position();
	int bombArea;
	
	
	public Message(String jsonString) throws JSONException{
		
		JSONObject message= new JSONObject(jsonString);
		type= message.getInt("type");
		switch(type){
		
			case ENTER: // A new player is entering the ring. I need to update the list
				String username= message.getString("username");
				String address= message.getString("networkaddress");
				int port= message.getInt("port");
				player= new PlayerInfo(username,address,port);
				break;
				
			case EAT: // a player has been eaten by me -> it's a response to my move
				// i need to give a point to my player
				username= message.getString("username");
				address= message.getString("networkaddress");
				port= message.getInt("port");
				player= new PlayerInfo(username,address,port);
				break;
			
			case BOMB: // a player comunicates a bomb! this comunication can be of 2 type. launched the bomb, or the bomb is exploded
				bombIsExploded= message.getBoolean("bombIsExploded");
				bombArea= message.getInt("bombArea");
				username= message.getString("username");
				address= message.getString("networkaddress");
				port= message.getInt("port");
				player= new PlayerInfo(username,address,port);			
				break;
			
			case TOKEN: // this is the special message. when i receive this message, i can move or launch bombs				
				break;
				
			case MOVE: // a player has moved the position. we need to check the position with ours
				
				username= message.getString("username");
				address= message.getString("networkaddress");
				port= message.getInt("port");
				player= new PlayerInfo(username,address,port);
				position.x= message.getInt("x");
				position.y= message.getInt("y");
				break;
				
			case EXIT: // a player is exiting the match because he's dead. to update the list
				username= message.getString("username");
				address= message.getString("networkaddress");
				port= message.getInt("port");
				player= new PlayerInfo(username,address,port);
				break;
				
			case OK: // response to the player movement. if he's not been eaten/esploded or in that position, it communicates that everything is allright for him
				//System.out.println("Ok message!");
				break;
			case WIN: // Someone is telling that he won
				username= message.getString("username");
				address= message.getString("networkaddress");
				port= message.getInt("port");
				player= new PlayerInfo(username,address,port);
				break;
				
			case EXPLODED: // A player is Exploded
				username= message.getString("username");
				address= message.getString("networkaddress");
				port= message.getInt("port");
				player= new PlayerInfo(username,address,port);
				break;
				
			case FIRSTPOSITION: // Someone is entering the game and he needs to know if his position is free
				username= message.getString("username");
				address= message.getString("networkaddress");
				port= message.getInt("port");
				player= new PlayerInfo(username,address,port);
				
				position.x= message.getInt("x");
				position.y= message.getInt("y");
				break;
			
			case ALREADYOCCUPIED: // A player is already in that position
				break;
		}
	}
	
	/** This is for the messages that don't need other Infos. The messages here are: TOKEN, OK, ALREADY OCCUPIED
	 * @param type
	 * @return
	 */
	public static String toJsonString(int type){
		JSONObject message= new JSONObject();
		try{
			message.put("type", type);
		}
		catch(Exception e){
			System.out.println("Eccezione nel creare il messaggio");
		}
		return message.toString();
	}
	
	/** This is for the messages that  need the player Info. The messages here are: ENTER, EXIT, EAT, EXPLODED
	 * 
	 * @param type
	 * @param player
	 * @return
	 */
	public static String toJsonString(int type, PlayerInfo player){
		JSONObject message= new JSONObject();
		try{
			message.put("type", type);
			message.put("username", player.getUsername());
			message.put("networkaddress", player.getAddress());
			message.put("port", player.getPort());
		}
		catch(Exception e){
			System.out.println("Eccezione nel creare il messaggio con giocatore o con EAT");
		}
		return message.toString();
	}
	
	// This is for the bomb message.
	public static String toJsonString(int type, boolean bomb, int bombArea, PlayerInfo player){
		JSONObject message= new JSONObject();
		try{
			message.put("type", type);
			message.put("bombIsExploded", bomb);
			message.put("bombArea", bombArea);
			//The player that has sent the bomb
			message.put("username", player.getUsername());
			message.put("networkaddress", player.getAddress());
			message.put("port", player.getPort());
		}
		catch(Exception e){
			System.out.println("Eccezione nel creare il messaggio per bomba");
		}
		return message.toString();
	}
	
	// This is for the positioning messages. The messages here are: MOVE, FIRSTPOSITION
		public static String toJsonString(int type, Position position, PlayerInfo player){
			JSONObject message= new JSONObject();
			try{
				message.put("type", type);
				message.put("x", position.x);
				message.put("y", position.y);
				message.put("username", player.getUsername());
				message.put("networkaddress", player.getAddress());
				message.put("port", player.getPort());
			}
			catch(Exception e){
				System.out.println("Eccezione nel creare il messaggio per move/ firstposition");
			}
			return message.toString();
		}	

}
