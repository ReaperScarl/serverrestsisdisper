package peerSisDisPer;

import simulation_src.*;
import java.util.List;

// This is the class responsible to check the outliers and to give the bombs to the IOmanager (IOThread)
public class BombManager extends Thread {
	Thread simulatorThread;
	BombBuffer buffer;
	IOThread iomanager;
	double EMA=0;
	// value of develpment
	float alfa= 0.7f;
	float th= 10f;

	
	//Contructor
	BombManager(IOThread manager){
		iomanager=manager;
		buffer=new BombBuffer();
		simulatorThread= new Thread(new AccelerometerSimulator(buffer));
	}

	public void run() // starts to check for outliers after 1 sec
	{
		//System.out.println("Inizia la bombmanager");
		simulatorThread.start();
		while(true){
			try{
				Thread.sleep(1000); // Sleeps for 1 sec
			}
			catch(Exception e){
				System.out.println("Eccezione dormendo in BombManager");
				e.printStackTrace();
			}
			checkForOutliers();
		}
	}
	
	// Method called every one second to check for the outliers
	void checkForOutliers(){
		List<Measurement> measures= buffer.readAllAndClean();

		//Calcolare il valore medio
		double average=0;
		for(int i=0; i<measures.size(); i++){
			average+=measures.get(i).getValue();
		}
		average/=measures.size();
		double newEMA=0;
		//Calcolare E.M.A
		if(EMA==0) // Just the first time
		{
			//System.out.println("Prima volta. Ema i-1=0");
			newEMA=alfa*average;
		}
		else{
			newEMA=EMA+(alfa*(average-EMA));
		}
		//System.out.println("newema: "+newEMA+" e ema:  "+EMA);
		if((newEMA-EMA)>th)
		{
			int bomb=(int)(EMA %4);
			iomanager.addABomb(bomb);
		}
		//System.out.println("differenza � : "+(newEMA-EMA));
		EMA=newEMA;
	}
}
