package peerSisDisPer;

import java.net.*;

// � il thread responsabile per il management del server del processo
public class ServerSocketForPeer extends Thread {

	ServerSocket mySocket=null;
	GamePeer peer=null;
	
	ServerSocketForPeer(GamePeer myPeer, int port){
		peer=myPeer;
		try{
			mySocket = new ServerSocket(port);
		}
		catch(Exception e){ // Exception management. If i have problem is, it's because some other servers is already using my port.
			//System.out.println("Eccezione creando server socket");
			//e.printStackTrace();
		}
	}
	
	public void run(){
		while(true) {
			//System.out.println("Dentro while server");
			try{
				Socket connectionSocket = mySocket.accept();
				//System.out.println("Dopo accept");
				ThreadServerSocket threadSocket= new ThreadServerSocket(connectionSocket, peer);
				threadSocket.start();
			}
			catch(Exception e){ // I couldn't accept. This means that my port is already used. therefore i'll say it and disconnect.
				System.out.println("Porta gi� usata! Disconessione. Riprova con una porta diversa!");
				//e.printStackTrace();
				try{
					peer.client.deleteMeFromMatch(peer.myMatch);
				}
				catch(Exception ebis){
					System.out.println("Eccezione Nel accept delete.");
					ebis.printStackTrace();
				}
				System.exit(0);
			}
			try{
				Thread.sleep(50); // the sleep gives the token message a moment to finish the round before it comes back.
			}
			catch(Exception e)
			{
				System.out.println("eccezione dello sleep del server");
			}
		}
	}
	
	void closeTheConnection(){
		try{
			mySocket.close();
		}
		catch( Exception e){
			System.out.println("Exception nel close");
		}
	}
}
