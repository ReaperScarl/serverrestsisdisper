
package peerSisDisPer;
import serverRestSisDisPer.*;
import serverRestSisDisPer.MatchesList;


import java.io.*;
import java.net.*;
import java.util.*;

import org.json.*;


public class ClientForRestServer {

	//The index page
	private static final String targetURL = "http://localhost:8080/serverRestSisDisPer/rest/Server/";
	boolean thereareactivematches=false;
	boolean isInsideMatch=false;
	
	MatchInfo [] matches;
	
	// -----  HTTP METHODS
	// --GET
	void startAGetRequest() throws Exception // gets the active matches
	{
		URL restServiceURL= new URL(targetURL);
		HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
		httpConnection.setRequestMethod("GET");
		httpConnection.setRequestProperty("Accept", "application/json");
		
		if( httpConnection.getResponseCode()!=200){
			throw new RuntimeException("HTTP GET Request Failed with Error: "+httpConnection.getResponseCode());
		}
		
		BufferedReader responseBuffer= new BufferedReader (new InputStreamReader(httpConnection.getInputStream()));
		String output="";
        String temp;
        while ((temp = responseBuffer.readLine()) != null) {
            output+=temp;
        }
        httpConnection.disconnect();
        
        // output is the info we need.
        if(MatchesList.typeOfMessage(output)!=MatchesList.OK)
        	System.out.println("Non ci sono match attivi. Puoi creare una nuova partita ");
        else {
        	letMeSeeTheActiveMatches(output);
        	thereareactivematches=true;
        }
	}
	
	//-- POST
	void startAMatch(PlayerInfo player) throws Exception{ //adds new match

		//Insert the data for the connection
		MatchInfo match = writeTheMatchInfo();
		match.addPlayer(player.getUsername(), player.getAddress(), player.getPort());
		
		// Now the connection
		String postURL=targetURL+"AddMatch";
		URL restServiceURL= new URL(postURL);
		HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
		httpConnection.setRequestMethod("POST");
		httpConnection.setRequestProperty("Content-Type", "application/json");
		httpConnection.setRequestProperty("Accept", "text/plain");
		httpConnection.setDoOutput(true);
		httpConnection.setDoInput(true);
         
		//Send the info
		OutputStream os = httpConnection.getOutputStream();
		os.write(match.toJSONString().getBytes());
		os.flush();
		if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : "+ httpConnection.getResponseCode());
        }
		//System.out.println("DEBUG_CLIENTFORREST: input for rest"+ match.toJSONString());
		//Read the response
		BufferedReader br = new BufferedReader(new InputStreamReader((httpConnection.getInputStream())));
		String output="";
		String temp;
		System.out.println("In attesa di risposta");
		while ((temp = br.readLine()) != null) {
            output+=temp;
        }		
		//System.out.println("DEBUG_ the output is : "+ output);
		//Check the response	
		if(MatchInfo.typeOfMessage(output)==MatchInfo.OK){
			System.out.println("Hai creato la partita e stai per entrarci...");
			List<PlayerInfo> players= new LinkedList<PlayerInfo>();
			players.add(player);
			isInsideMatch=true;
			new GamePeer(match,this,players, player);
		}		
		if(MatchInfo.typeOfMessage(output)==MatchInfo.MATCHEXISTS)
			System.out.println("Riprova. La partita che stai tentando di creare esiste gi�");	
		if(output.contains("Retry"))
			System.out.println("Bad input");
		
        httpConnection.disconnect();
	}
	
	// -- PUT
	//Add the player to the match index
	void AddThePlayer(int index, PlayerInfo player) throws Exception{
		
		String putURL=targetURL+"AddPlayer";
		URL restServiceURL= new URL(putURL);
		HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();		
		httpConnection.setRequestMethod("PUT");
		httpConnection.setRequestProperty("Content-Type", "application/json");
		httpConnection.setRequestProperty("Accept", "application/json");
		httpConnection.setDoOutput(true);
		httpConnection.setDoInput(true);
		
		//Sends the match info with the player to add
		matches[index].addPlayer(player.getUsername(), player.getAddress(), player.getPort());
		//Send the info
		OutputStream os = httpConnection.getOutputStream();
		os.write(matches[index].toJSONString().getBytes());
		os.flush();
		//System.out.println("DEBUG_CLIENTFORREST: input for rest"+ matches[index].toJSONString());
		if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : "+ httpConnection.getResponseCode());
        }
		
		//Read the response
		BufferedReader br = new BufferedReader(new InputStreamReader((httpConnection.getInputStream())));
		String output="";
		String temp;
		System.out.println("In attesa di risposta...");
		while ((temp = br.readLine()) != null) {
            //System.out.println(temp);
            output+=temp;
        }
		//System.out.println("DEBUG_ the output is : "+ output);
		
		if(MatchInfo.typeOfMessage(output)==MatchInfo.PLAYERINSIDE){
			System.out.println("Errore! C'� gi� qualcuno dentro questa partita con lo stesso nome! prova un'altra partita!");
			return ;
		}			
		if(MatchInfo.typeOfMessage(output)==MatchInfo.MATCHNOTFOUND){
			System.out.println("Riprova! La partita � gi� finita");
			return ;
		}
		List<PlayerInfo> players;
		try // if you are here, you should have a right output
		{
			 players= MatchInfo.playersFromJson(output);
			 System.out.println("i giocatori sono: ");
	 
			for (int i=0; i<players.size(); i++){
				System.out.println(players.get(i).getUsername());
			}
			System.out.println("Stai entrando in partita...");
			isInsideMatch=true;
			new GamePeer(matches[index],this,players,player);
		}
		catch(JSONException e){
			System.out.println("Errore! il JSON non corrisponde ai players");
		}
		 httpConnection.disconnect();
	}
	
	// ---- DELETE
	void deleteMeFromMatch(MatchInfo match) { // I pass directly all the info it has to give (match and player)
		
		String deleteURL=targetURL+"DeletePlayer";
		try{
			URL restServiceURL= new URL(deleteURL);
			HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
			httpConnection.setRequestMethod("DELETE");
			httpConnection.setRequestProperty("Content-Type", "application/json");
			httpConnection.setRequestProperty("Accept", "text/plain");
			httpConnection.setDoOutput(true);
			httpConnection.setDoInput(true);
			
			//Sends the match info with the player to delete
			OutputStream os = httpConnection.getOutputStream();
			os.write(match.toJSONString().getBytes());
			os.flush();
			
			if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
	            throw new RuntimeException("Failed : HTTP error code : "+ httpConnection.getResponseCode());
	        }
			
			//Read the response
			BufferedReader br = new BufferedReader(new InputStreamReader((httpConnection.getInputStream())));
			String output="";
			String temp;
			while ((temp = br.readLine()) != null) {
	            //System.out.println(temp);
	            output+=temp;
	        }
			
			if(MatchInfo.typeOfMessage(output)==MatchInfo.MATCHNOTFOUND){
				System.out.println("Errore! La partita � stata gi� eliminata, ma grazie per aver giocato"); // succede, per esempio, quando il server si riavvia ma sei ancora in partita
				System.exit(0);
				return ;
			}
			if(MatchInfo.typeOfMessage(output)==MatchInfo.PLAYERNOTFOUND){  // shouldn't happen
				System.out.println("Errore! Sei gi� stato eliminato");
				System.exit(0);
				return ;
			}
			if(MatchInfo.typeOfMessage(output)==MatchInfo.OK){
				//System.out.println("Sei stato eliminato. Grazie per aver giocato e alla prossima!");
				System.exit(0);
			}
		}
		catch (Exception e){
			System.out.println("Eccezione nel delete from match");
			e.printStackTrace();
		}
	}

	
	//   -- INTERNAL methods
	
	// show the matches details
	void letMeSeeTheActiveMatches(String jsonString) throws Exception{
		
		MatchesList list= new MatchesList();
		this.matches = list.fromJson(jsonString);
		
		System.out.println("Le partite attive sono:");
		for (int i=0; i<matches.length; i++)
			System.out.println(i+1+") Nome partita: "+this.matches[i].getMatchName()+" ,      lato griglia: "+matches[i].getLength()+" ,      punteggio per vincere: "+matches[i].getPoints());
	}
	
	// write the info on a matchInfo
	MatchInfo writeTheMatchInfo(){
		// First: Add a name, a length and a side
		BufferedReader input= new BufferedReader( new InputStreamReader(System.in));
		String matchname=null;
		int length=0;
		int points=0;
		boolean correctinput=false;
		try{
			while(!correctinput){ // name control
				System.out.println("Inserisci un nome per la partita");
				matchname= input.readLine();
				if(matchname!=null && matchname.length()>0)
					correctinput=true;
				else
					System.out.println("Errore! devi inserire almeno un carattere");
			}
		}
		catch(Exception e){
			System.out.println("Errore di input");
		}
		
		correctinput=false;
		while(!correctinput){ // length control
			System.out.println("Inserisci la lunghezza del lato della griglia");
			try{
				length= Integer.parseInt(input.readLine());
				if(length>0&& length%2==0)
					correctinput=true;
				else
					System.out.println("Errore! La lunghezza deve essere maggiore di 1 e deve essere pari!");
			}
			catch(Exception e){
				System.out.println("Errore! La lunghezza deve essere un valore numerico");
			}
		}
		
		correctinput=false;
		while(!correctinput){ // points to win control
			System.out.println("Inserisci il punteggio per vincere");
			try{
				points= Integer.parseInt(input.readLine());
				if(points>0)
					correctinput=true;
				else
					System.out.println("Errore! Bisogna fare almeno 1 punto per vincere");
			}
			catch(Exception e){
				System.out.println("Errore! il punteggio da raggiungere deve essere un valore numerico");
			}
		}	
		return new MatchInfo(matchname,length, points);
	}
}
