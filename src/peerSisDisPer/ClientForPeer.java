package peerSisDisPer;

import java.io.*;
import java.net.*;

// Class responsible for the send of the messages to other sockets
public class ClientForPeer extends Thread {
	
	Socket clientSocket=null;
	Message message;
	String output;
	int type;
	// takes a port of the socket it has to talk with
	ClientForPeer(String address,int port, String output, int type){
		
		try{
			clientSocket = new Socket(address, port);
			
		}
		catch(Exception e)
		{
			System.out.println("Eccezione durante la creazione di client socket. Il player � uscito?");
			//e.printStackTrace();
		}
		this.output=output;
		this.type=type;
		
	}
	
	
	public void run(){
		if(clientSocket==null){
			System.out.println(" messaggio mandato a qualcuno che ha forse chiuso. Da eliminare il processo!");
			return;
		}
		
		switch(type){
		case 0: //send token
			
			sendToken(output);
			break;
		
		case 1: // send without response
			
			sendMessageWithoutResponse(output);
			break;
				
		}
		closeTheConnection();
	}
	
	//Tries to close the connection of the client
	void closeTheConnection(){
		try {
			clientSocket.close();
		} catch (IOException e) {
			System.out.println("eccezione chiudendo client socket");
		}
	}
	
	// This should send the message. In base of what the response is, it has to do something
	int sendMessageWithResponse(String jsonMessage){ 
		//System.out.println("Sending with response");
		if(clientSocket==null){
			System.out.println("Da eliminare il processo!");
			return Message.OK; // se non c'� � come se mi dasse l'ok
		}	
		String input="";
		try{
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());	
			outToServer.writeBytes(jsonMessage+'\n');
			//System.out.println("Inviato il messaggio");
		}
		catch(IOException e){
			System.out.println("eccezione Inviando/ricevendo un messaggio");
			e.printStackTrace();
		}
		//Input is the jsonString that i need with the response
		try{
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			input= inFromClient.readLine();
			//System.out.println("the input is: "+input);
			 message= new Message(input);
			return message.type;
		}
		catch(Exception e){
			System.out.println("eccezione sul json ricevuto");
			e.printStackTrace();
		}
		System.out.println("You shouldn't be here");
		return -1; 
	}

	// This message doesn't need a response. used only for the token
	void sendToken(String jsonMessage){
		try{
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			outToServer.writeBytes(jsonMessage+'\n');
		}
		catch(Exception e){
			System.out.println("eccezione Inviando il token");
			e.printStackTrace();
		}
	}
	
	// This message doesn't need a response. has to send the other messages (EAT, OK, etc)
	void sendMessageWithoutResponse(String jsonMessage){
		//System.out.println("Send with no response");
		try{
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			outToServer.writeBytes(jsonMessage+'\n');
		}
		catch(IOException e){
			System.out.println("eccezione Inviando il messaggio senza risposta");
			e.printStackTrace();
		}
	}
}
