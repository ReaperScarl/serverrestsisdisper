package peerSisDisPer;

import serverRestSisDisPer.PlayerInfo;

import java.io.*;

public class MainPeer {

	String address;
	static String  serverpath= "/serverRestSisDisPer/";
	
	public static void main(String[] args) throws Exception {
		PlayerInfo myplayer= new PlayerInfo();
		myplayer.askStartingInfo();
		ClientForRestServer client= new ClientForRestServer();
		System.out.println("Benvenuto in MMOG.");
		try{
			
			client.startAGetRequest();
		}
		catch(Exception e){
			System.out.println("Il server non � al momento raggiungibile, riprova pi� tardi!");
			System.exit(0);
		}
		
		BufferedReader input= new BufferedReader( new InputStreamReader(System.in));
		
		boolean inputcorrect=false;
		while(!inputcorrect|| !client.isInsideMatch) // per controllare che l'input del giocatore sia corretto
		{
			if(client.thereareactivematches)
				System.out.println("Cosa vuoi fare? Per scegliere and entrare in una partita digita il numero corrispondente, 0 per crearne una nuova, q per terminare");
			else
				System.out.println("Cosa vuoi fare? Scegli  0 per creare una nuova partita, q per terminare");
			
			inputcorrect=false;
			char c='x';
			String temp=input.readLine();
			if(temp!=null && temp.length()>0)
				c= temp.charAt(0); // legge solo il primo carattere
			if(c=='q'||c=='Q'){
				inputcorrect=true;
				System.out.println("Hai deciso di terminare. Alla prossima!");
				System.exit(0);
				break;
			}
			if(c=='0'){
				inputcorrect=true;
				System.out.println("Hai deciso di creare una partita.");
				client.startAMatch(myplayer);
			}
			if(client.thereareactivematches){
				try{
					int match= Integer.parseInt(temp);
					if(match>0 && match<=client.matches.length){
						System.out.println("Hai deciso di entrare dentro la partita "+client.matches[match-1].getMatchName()); // can change with the name of the match, instead of the number
						client.AddThePlayer(match-1, myplayer);
						inputcorrect=true;
					}	
				}
				catch(Exception e){
					
				}
			}
			if(!inputcorrect)
				System.out.println("L'input inserito non � corretto. Riprova");
			
			if(!client.isInsideMatch){
				input= new BufferedReader( new InputStreamReader(System.in));
			}
		}
	}
}
