package peerSisDisPer;

import serverRestSisDisPer.*;
import java.util.List;

// Class that is responsible for the behaviour of the peer in game
public class GamePeer {

	ClientForRestServer client;
	List<PlayerInfo> players;
	PlayerInfo myPlayer;
	PlayerInfo nextPlayer;
	MatchInfo myMatch;
	Map mapInfo;
	IOThread ioManager;
	ClientsPeerManager clientsmanager;
	int myscore=0;
	int whereAmI=-1;
	boolean beeneaten=false;
	boolean imexploded=false;
	int playersExploded;
	
	boolean alreadydeleted=false;
	
	// Takes the infos necessary
	GamePeer(MatchInfo match, ClientForRestServer client, List<PlayerInfo> players, PlayerInfo myplayer){
		
		this.client=client; // for the deleting at the end
		this.players=players; // to update the others lists
		this.myPlayer=myplayer; // to know who i am
		this.myMatch=match; // To know the info about the current match
		mapInfo=new Map(match.getLength()); // Info to know where i am

		clientsmanager=new ClientsPeerManager(players);
		
		System.out.println("Entrato in partita!");
		
		ServerSocketForPeer myServer= new ServerSocketForPeer(this, myplayer.getPort());
		myServer.start();
		ioManager=new IOThread();
		
		if(players.size()==1)   {
			
			nextPlayer=players.get(0);
			clientsmanager.sendToken(nextPlayer);
			whereAmI=0;
		}	
		else{
			// Send my player to the others
			
			for(int i=0; i<players.size(); i++){
				if(players.get(i).getUsername().equals(myplayer.getUsername())){
					whereAmI=i;
				}	
			}
			if(whereAmI>=0){
				// it takes as next the next in the line with the module, so the last has as next the first
				nextPlayer=(players.get((whereAmI+1)% players.size())); 
				//System.out.println("my next is: "+(whereAmI+1)%players.size());
				clientsmanager.sendEnter_ExitMessage(myplayer, true);
			}	
		}	
	

		clientsmanager.sendFirstPosition(myPlayer, this);
		System.out.println(mapInfo.toString());
		ioManager.start();
	}
	
	// I need to update the list for the player inside. if isEntering, i need to insert him, otherwise i need to delete him
	void updateThePlayerList(PlayerInfo player, boolean isEntering ){
		synchronized(players)
		{
			if(isEntering){
				players.add(player);
			}	
			else{			// not great in performance, but the remove.player didn't work
				for(int i=0; i<players.size(); i++){
					//System.out.println(players.get(i).getUsername());
					if(players.get(i).getUsername().equals(player.getUsername())){
						players.remove(i);
					}	
				}
			}	
			clientsmanager.updateList(players);
			//Update my next
			//System.out.println("Dopo l'update, ci sono: "+players.size());
			for(int i=0; i<players.size(); i++){
				//System.out.println(players.get(i).getUsername());
				if(players.get(i).getUsername().equals(myPlayer.getUsername())){
					whereAmI=i;
					//System.out.println("sono in "+i);
				}	
			}
			if(whereAmI>=0){
				// it takes as next the next in the line with the module, so the last has as next the first
				nextPlayer=(players.get((whereAmI+1)%players.size())); 
				//System.out.println("My next is: "+(whereAmI+1)%players.size());
			}
		}
	}
	
	//Called by the server. I check if my position with the external position. if it is = then i've been eaten
	void checkThePosition(Position position, PlayerInfo player){
		
		//System.out.println("Sono in check position");
		if(mapInfo.myPosition.x==position.x &&mapInfo.myPosition.y==position.y){
			System.out.println("Sei stato mangiato");
			// Send the message of EAT to the eater
			beeneaten=true;
			clientsmanager.sendRespondForMovement(Message.EAT, myPlayer, player);
		}
		else{
			// To Delete it
			//System.out.println("DEBUG- GAMEPEER.checkPosition. my pos: "+mapInfo.myPosition.x+" , "+mapInfo.myPosition.y+
					//" e quelle dell'avversarsio: "+position.x+" , "+position.y);
			//System.out.println("Dovrei inviare il Message.ok");
			clientsmanager.sendRespondForMovement(Message.OK,null, player);
		}
	}
	
	// check if i have to do moves
	void checkOnToken(){
		
		//System.out.println("Ho il token!");
		if(ioManager.needToMove)
		{
			System.out.println(" fatto!");
			if(ioManager.decision!='b')
			{
				mapInfo.changeMyPosition(ioManager.decision);
				System.out.println("Ti sei mosso! \n"+mapInfo.toString());
				//Send the move to everyone
				clientsmanager.sendMove(mapInfo.myPosition, myPlayer, this);
			}
			else{
				
				//Invia messaggio con la bomba
				clientsmanager.sendBomb(false, ioManager.bombarea, myPlayer,this);
				DelayThreadForBomb bombthread= new DelayThreadForBomb(ioManager.bombarea, myPlayer,clientsmanager,this);
				System.out.println("Stai lanciando una bomba nell'area "+Map.areas[ioManager.bombarea]);
				bombthread.start();
				// Fare partire un Thread che dopo 5 secondi comunica che la bomba � esplosa
				ioManager.removeFirstBomb();
				
			}
			ioManager.needToMove=false;
			synchronized(ioManager){
				ioManager.notify();
			}
		}

		synchronized(players) //Send the token
		{
			clientsmanager.sendToken(players.get((whereAmI+1)%players.size()));
			nextPlayer=players.get((whereAmI+1)%players.size());
			//System.out.println("Token passato");
		}
		if(imexploded){
			beeneaten=true;
			eaten();
		}
	}
	
	// Ho fatto 1 punto! controllo se ho raggiunto il punteggio per vincere
	void increaseMyScore() throws Exception{
		myscore++;
		System.out.println("Il tuo punteggio � ora a "+myscore);
		if(myscore>=myMatch.getPoints()) // Ho vinto!
		{
			ioManager.imactive=false;
			ioManager.interrupt();
			// Devo inviare che ho vinto a tutti
			clientsmanager.sendWin(myPlayer, this);
			System.out.println("Complimenti! hai vinto!");
			client.deleteMeFromMatch(myMatch);
		}
	}
	
	// Checks  if the bomb is in my area
	void checkBomb(int bombarea, PlayerInfo player){
		
		if(mapInfo.getMyArea()==bombarea){
			System.out.println("Una bomba � esplosa nella tua area!");
			clientsmanager.sendRespondForBomb(Message.EXPLODED, myPlayer, player);
			beeneaten=true;
			ioManager.imactive=false;
			ioManager.interrupt();
		}
		else{
			clientsmanager.sendRespondForBomb(Message.OK, myPlayer, player);
		}
	}
	
	
	// Qualcun altro ha vinto, quindi devi chiudere la partita. Non mi cancello dalla lista perch� devono uscire tutti, quindi la parte da cancellare � solo sul server per eliminare la partita
	void someoneWon(String username){
		System.out.println(username+" ha vinto la partita! Grazie per aver giocato e alla prossima!");
		try{
			client.deleteMeFromMatch(myMatch);
		}
		catch(Exception e ){
			System.out.println("Eccezione del cancellarmi");
			e.printStackTrace();
		}
	}
	
	// I've been eaten or in any way dead, then i need to exit
	void  eaten(){
		// the already deleted is for the rare case in which 
		//the server doesn't respond immediately and the token is still going around in the ring composed by 1 member
		if(beeneaten&&!alreadydeleted){ 
			clientsmanager.sendEnter_ExitMessage(myPlayer, false);
			try{
				Thread.sleep(1500);
				alreadydeleted=true;
				System.out.println("Grazie per aver giocato e alla prossima!");
				//System.out.println("DEBUG_ eaten. prima di delete my match!");
				client.deleteMeFromMatch(myMatch);
			}
			catch(Exception e){
				System.out.println("Eccezione nel eaten");
				e.printStackTrace();
			}
		}
		else
			System.exit(0);
	}
	
	synchronized void  enemiesExplosions( ){
		if(playersExploded<3)
			playersExploded++;
	}
	
}
