package peerSisDisPer;
import java.io.*;
import java.net.*;

// Thread responsible to listen to the other peers
public class ThreadServerSocket extends Thread {

	private Socket connectionSocket = null;
	private GamePeer myPeer;
	BufferedReader inFromClient =null;
	
	public ThreadServerSocket(Socket s, GamePeer peer) {
		connectionSocket = s;
		myPeer=peer;
	}
	
	
	public void run(){

		try{
			inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			//System.out.println("start del server socket thread");

			String input="";
			input+=inFromClient.readLine();
			Message message= new Message(input);
			
			//System.out.println("input: "+input);

			switch (message.type){
			
			// A Bomb Message
			case Message.BOMB:
				if(!message.bombIsExploded){
					System.out.println("Una bomba esploder� nell'area "+Map.areas[message.bombArea]+" tra 5 secondi!");
				}
				else{
					System.out.println("Una bomba � esplosa nell'area "+Map.areas[message.bombArea]+"!");
					myPeer.checkBomb(message.bombArea, message.player);

					if(myPeer.beeneaten)
						myPeer.eaten();
				}
				break;
				
				// a EAT message. i stepped on someone
			case Message.EAT:
				synchronized(myPeer.clientsmanager){
					myPeer.clientsmanager.notify();
				}
				myPeer.updateThePlayerList(message.player, false); // so gi� che lui non lo avr�, quindi lo elimino direttamente
				System.out.println("Hai mangiato "+message.player.getUsername()+". Prendi un punto!");
				myPeer.increaseMyScore();
				break;
	
			// The opponent player has moved. i need to check my position
			case Message.MOVE: 
				//System.out.println("DEBUG- THREADSERVER.run message.player: "+message.player.getUsername()+" e my player: "+myPeer.myPlayer.getUsername());
				myPeer.checkThePosition(message.position, message.player);

					// If i've been eaten, i Send the message of Exit to everyone (and let know to take a point)
				if(myPeer.beeneaten)
					myPeer.eaten();
				break;
				
				// OK Message. different cases: 
				// --Respond to move and bomb -> gives the ok to say that the peer is fine and u can continue
				// --Respond to firstPosition -> gives the ok to say that the position is free (notify wasted but doesn't make errors)
			case Message.OK: 

				synchronized(myPeer.clientsmanager){
					myPeer.clientsmanager.notify();
				}
				break;
				
			//A player has entered the ring and the match	
			case Message.ENTER:
				myPeer.updateThePlayerList(message.player, true);
				break;
				
			//A player has Exited the ring and the match	
			case Message.EXIT:
				//System.out.println("Dentro exit");
				myPeer.updateThePlayerList(message.player, false);
				break;
				
			// I received the token, i'll check if i need to do something
			case Message.TOKEN:
				//System.out.println("preso il token");
				myPeer.checkOnToken();
				break;
			
			// I received a Win message. therefore i lost
			case Message.WIN:
				myPeer.someoneWon(message.player.getUsername());
				break;
				// Someone just entered and wants to know if a position is free. If i'm not occupying it, i'll say ok
			case Message.FIRSTPOSITION: 
				//System.out.println("Ricevuto first position");
				DataOutputStream outToServer = new DataOutputStream(connectionSocket.getOutputStream());	
				String jsonMessage;
				if(myPeer.mapInfo.getMyPosition().x==message.position.x && myPeer.mapInfo.getMyPosition().y==message.position.y)
					jsonMessage=Message.toJsonString(Message.ALREADYOCCUPIED);
				else
					jsonMessage=Message.toJsonString(Message.OK);
				//System.out.println("output: "+jsonMessage);
				outToServer.writeBytes(jsonMessage+'\n');
				break;
				
			// Someone is telling me that he's exploded
			case Message.EXPLODED:
				//System.out.println("ThreadServer: exploded ");
				myPeer.enemiesExplosions();
				synchronized(myPeer.clientsmanager){
					myPeer.clientsmanager.notify();
				}
				myPeer.updateThePlayerList(message.player, false); // so gi� che lui non lo avr�, quindi lo elimino direttamented
				break;
			}
			//System.out.println("Fine thread");
		}
		// management of the exception. 
		//When i'm here it's because i couldn't read a message. that's cause by a wrong connection when someone is using the same port as mine.
		// I'll proceed like nothing happened, because if he couldn't send anything means he's not in the ring
		catch(Exception e){ 
			
			//System.out.println("Eccezione della lettura nel server peer");
			//e.printStackTrace();
		}
	}
}
