package peerSisDisPer;

// Class with the map infos and the position of the peer
public class Map {

	int length;
	Position myPosition;
	static String[] areas={"verde","rossa", "blue", "gialla"};

	// Should insert the player inside the map
	Map(int length){
		myPosition= new Position();
		//i'm outside the map. after i know that my position is free, i'll move on the new position.
		myPosition.x=-1;
		myPosition.y=-1;
		this.length=length;
	}

	Position getMyPosition(){
		return myPosition;
	}
	
	// Translates the movemente of the player
	void changeMyPosition(char dir){
		
		if(dir=='w')
				myPosition.y=(myPosition.y+1)%length;
		if(dir=='s')
			myPosition.y=(length+myPosition.y-1)%length;
		if(dir=='a')
			myPosition.x=(length+myPosition.x-1)%length;
		if(dir=='d')
			myPosition.x=(myPosition.x+1)%length;
	}
	
	public String toString(){
		return "La tua posizione � ["+myPosition.x+","+myPosition.y+"] , Area "+areas[getMyArea()];
	}
	
	public int getMyArea(){
		if(myPosition.x==-1) // i'm not in the map yet
			return -1;
		if(myPosition.x<length/2){
			if(myPosition.y>=length/2)
				return 0;
			else
				return 2;
		}
		else{
			if(myPosition.y>=length/2)
				return 1;
			else
				return 3;
		}
	}
}
