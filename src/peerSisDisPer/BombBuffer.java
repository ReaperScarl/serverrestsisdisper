package peerSisDisPer;

import simulation_src.*;
import java.util.*;

// Class implementing the interface of the buffer
public class BombBuffer implements Buffer{

	List<Measurement> measures= new LinkedList<Measurement>();
	
	public void addNewMeasurement(Measurement measure){
		synchronized(measures){
			//System.out.println("Added a measure: "+measure.getValue());
			measures.add(measure);
		}
	}
	
	public List<Measurement> readAllAndClean(){
		synchronized(measures){
			List<Measurement> measurestoSend= new LinkedList<Measurement>();
			
			for(int i=0; i<measures.size(); i++){
				measurestoSend.add(measures.get(i));
			}
			measures.clear();
			//System.out.println("Dopo clear - il numero di measurestoSend �: "+measurestoSend.size());
			//System.out.println("read all");
			return measurestoSend;
		}
	}

}
