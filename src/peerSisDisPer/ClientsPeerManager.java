package peerSisDisPer;
import serverRestSisDisPer.*;
import java.util.*;


// This manages the clients when it's needed to send informations to others (like ENTER, EXIT, MOVE, BOMB)
public class ClientsPeerManager {
	
	List<PlayerInfo> players;
	
	ClientsPeerManager(List<PlayerInfo> list){
		players=list;
	}
	
	//Updates the list. Called by the GamePeer
	public void updateList(List<PlayerInfo> list){
		synchronized(players){
			players=list;
		}
	}
	
	//Has to send the Token to the next in the ring
	void sendToken(PlayerInfo player){
		//System.out.println("Sto per inviare un token");
		String output= Message.toJsonString(Message.TOKEN);
		ClientForPeer client= new ClientForPeer(player.getAddress(),player.getPort(), output,0);
		client.start();
		//System.out.println("Ho inviato il token a "+ player.getUsername());
	}
	
	//Has to tell to everyone in the ring to update the list. Called in Game Peer. No need for a response
	public void sendEnter_ExitMessage(PlayerInfo player,boolean isEntering){
		
		if(isEntering){
			//System.out.println("Entering in send enter");
			String output= Message.toJsonString(Message.ENTER, player);
			for(int i=0; i<players.size(); i++){
				if(!players.get(i).getUsername().equals(player.getUsername())){ // DOn't send it to the player that is sending them
					ClientForPeer client= new ClientForPeer(players.get(i).getAddress(),players.get(i).getPort(), output,1);
					client.start();
				}
			}
		}
		else{
			//System.out.println("Entering in send exit");
			String output= Message.toJsonString(Message.EXIT, player);
			for(int i=0; i<players.size(); i++){
				if(!players.get(i).getUsername().equals(player.getUsername())){
					ClientForPeer client= new ClientForPeer(players.get(i).getAddress(),players.get(i).getPort(), output,1);
					client.start();
				}
			}
		}
	}
	
	// Has to send to the others my new position
	public void sendMove(Position mypos, PlayerInfo myplayer,GamePeer mypeer){
		String output= Message.toJsonString(Message.MOVE, mypos, myplayer);
		//System.out.println("output in send move: "+output);

			int playerSize=players.size();
			
			for(int i=0; i<players.size(); i++){
				//System.out.println("inside for synch players.size: "+players.size());
				if(!players.get(i).getUsername().equals(myplayer.getUsername())){
					//System.out.println("players i: "+players.get(i).getUsername()+" e my player: "+myplayer.getUsername());
					ClientForPeer client= new ClientForPeer(players.get(i).getAddress(),players.get(i).getPort(), output,1);
					//System.out.println(" after the client call");
					client.start();
				}
			}
			if(players.size()<playerSize) // in case of a cuncurrent exit
				playerSize=players.size();
			for(int i=0; i<playerSize-1; i++){
				//System.out.println("inside synch for wait players.size: "+players.size());
					
				//System.out.println("Entro nel for:  "+i);
				try{
					synchronized(this){
						this.wait();
						//System.out.println("Svegliato");
					}
				}
				catch(Exception e){
					System.out.println("Eccezione nel wait del client");
				}
			}
	}
	
	
	// Has to send the info about the bomb
	public void sendBomb(boolean isExploded,int bombarea, PlayerInfo myPlayer,GamePeer mypeer){
		String output= Message.toJsonString(Message.BOMB,isExploded, bombarea, myPlayer);
		//System.out.println("is exploded "+isExploded);

		for(int i=0; i<players.size(); i++){
			
			if(!players.get(i).getUsername().equals(myPlayer.getUsername())){
				ClientForPeer client= new ClientForPeer(players.get(i).getAddress(),players.get(i).getPort(), output,1);	
				client.start();
			}
		}
		
		if(isExploded){ // the bomb is Exploded, so i need to check a response from the other players
			for(int i=0; i<players.size(); i++){
				if(!players.get(i).getUsername().equals(myPlayer.getUsername())){
					try{
						synchronized(this){
							this.wait();
							//System.out.println("Svegliato");
						}
					}
					catch(Exception e){
						System.out.println("Eccezione nel wait del client");
					}
				}
			}
			if(mypeer.mapInfo.getMyArea()==bombarea || mypeer.imexploded){
				System.out.println("La bomba � caduta sulla tua area!");
				mypeer.imexploded=true;
				mypeer.ioManager.imactive=false;
				mypeer.ioManager.interrupt();
				
			}
			else{
				if(mypeer.playersExploded>0){
					System.out.println("Complimenti, hai fatto "+mypeer.playersExploded+" punti!");
					for(int i=0; i<mypeer.playersExploded; i++)
					{
						try{
							mypeer.increaseMyScore();
						}
						catch(Exception e)
						{
							System.out.println("Eccezione nell'increasemyscore");
						}
					}
					mypeer.playersExploded=0;	
				}	
			}
		}
	}
	
	// Send the position. until he hasn't a free position, he doesn't enter the map
	public void sendFirstPosition(PlayerInfo myplayer, GamePeer myPeer){
		Position pos=new Position();
		pos.randomPos(myPeer.mapInfo.length);
		String output= Message.toJsonString(Message.FIRSTPOSITION, pos, myplayer);
		//System.out.println("before synch players.size: "+players.size());

			boolean positionOkForAll;
			do{
				positionOkForAll=true;
				
				for(int i=0; i<players.size(); i++){
					//System.out.println("players.size: "+players.size());
					if(!players.get(i).getUsername().equals(myplayer.getUsername())){
						ClientForPeer client= new ClientForPeer(players.get(i).getAddress(),players.get(i).getPort(), output,2);
						//System.out.println(" after the client call");
						if(client.sendMessageWithResponse(output)== Message.OK){
							client.closeTheConnection();
						}
						else{ // Already OCCUPIED
							positionOkForAll=false;
							break;
						}
					}
				}
				if(positionOkForAll==false){ // significa che devo scegliere un'altra posizione

					pos.randomPos(myPeer.mapInfo.length);
					output= Message.toJsonString(Message.FIRSTPOSITION, pos, myplayer);
				}
				
			}while(!positionOkForAll);
			myPeer.mapInfo.myPosition=pos;
	}
	
	//Has to send the win token
	public void sendWin(PlayerInfo myPlayer, GamePeer myPeer){
		String output= Message.toJsonString(Message.WIN, myPlayer);
		
		for(int i=0; i<myPeer.players.size(); i++){
			if(!players.get(i).getUsername().equals(myPlayer.getUsername())){
				ClientForPeer client= new ClientForPeer(myPeer.players.get(i).getAddress(),myPeer.players.get(i).getPort(), output,1);
				client.start();
			}
		}		
	}
	
	// 2 cases: EAT or OK
	public void sendRespondForMovement(int type,PlayerInfo myplayer, PlayerInfo movedPlayer){
		if(type==Message.OK){
			String output= Message.toJsonString(Message.OK);
			ClientForPeer client= new ClientForPeer(movedPlayer.getAddress(),movedPlayer.getPort(), output,1);
			client.start();
			//System.out.println("After the sent of the OK");
		}
		else{
			String output= Message.toJsonString(Message.EAT, myplayer );
			ClientForPeer client= new ClientForPeer(movedPlayer.getAddress(),movedPlayer.getPort(), output,1);
			client.start();
		}
	}
	
	// 2 cases: EXPLODED or OK
	public void sendRespondForBomb(int type, PlayerInfo myplayer, PlayerInfo bombPlayer){
		if(type==Message.OK){
			String output= Message.toJsonString(Message.OK);
			ClientForPeer client= new ClientForPeer(bombPlayer.getAddress(),bombPlayer.getPort(), output,1);
			client.start();
			//System.out.println("After the sent of the OK");
		}
		else{
			String output= Message.toJsonString(Message.EXPLODED, myplayer );
			ClientForPeer client= new ClientForPeer(bombPlayer.getAddress(),bombPlayer.getPort(), output,1);
			client.start();
		}
	}
}
