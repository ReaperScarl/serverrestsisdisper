package peerSisDisPer;

import serverRestSisDisPer.PlayerInfo;

// Thread that manages to send the bomb message after 5 secs
public class DelayThreadForBomb  extends Thread{

	int delay=5000; // 5 secs
	int bombArea;
	PlayerInfo player;
	ClientsPeerManager manager;
	GamePeer myPeer;
	
	DelayThreadForBomb(int bombArea, PlayerInfo myPlayer, ClientsPeerManager clientman, GamePeer peer){
		this.bombArea=bombArea;
		player=myPlayer;
		manager=clientman;
		myPeer=peer;
	}

	public void run(){
		try{
			Thread.sleep(delay);
		}
		catch(Exception e){
			System.out.println("Eccezione nello sleep del bomb delay");
		}
		System.out.println("Sto per far esplodere la bomba nell'area "+Map.areas[bombArea]); // i can see it only on the sender screen
		manager.sendBomb(true, bombArea, player,myPeer);
	}
}
