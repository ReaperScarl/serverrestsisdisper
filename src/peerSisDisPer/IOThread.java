package peerSisDisPer;
import java.io.*;
import java.util.*;

// This class should be responsible for the IO of the peer.
// It should check if the player has inserted a correct choice and wait for the token
public class IOThread extends Thread {
	
	boolean needToMove=false;
	char decision; // w a s d for movement and b for bomb
	boolean bombAvailable=false;
	int bombarea;
	boolean imactive=true;
	List<Integer> bombs= new LinkedList<Integer>();
	BombManager bombManager;
	
	public void run(){
		
		boolean correctinput=false;
		bombManager= new BombManager(this);
		bombManager.start();
		while(imactive){
			
			try{
				while(!correctinput){ // choice control
					BufferedReader input= new BufferedReader( new InputStreamReader(System.in));
					System.out.print("Seleziona una mossa da giocare: ");
					if(bombAvailable){
						if(bombs.size()>0)
							bombarea= bombs.get(0).intValue();
						System.out.println("w= su, s= giu', a= sinistra, d= destra, b= lancia bomba nell'area "+Map.areas[bombarea]); // da inserire l'area
					}	
					else
						System.out.println("w= su, s= giu', a= sinistra, d= destra");
					String string=input.readLine();
					if(string!=null && string.length()>0){
						char temp= string.charAt(0);
						if((int)temp>48 && checkCorrectDecision(temp)){
							correctinput=true;
							decision=temp;
							needToMove=true;
						}	
						else
							System.out.println("Errore! puoi inserire solo i caratteri elencati");
					}
					else
						System.out.println("Errore! Devi inserire un carattere!");
				}
			}
			catch(Exception e){
				System.out.println("Errore di input");
				e.getStackTrace();
			}
			
			if(correctinput){
				System.out.print("In attesa di comunicare la mossa...");
				try{
					synchronized(this){
						this.wait(); // after the move is correct, it can go to sleep
						//System.out.println("Risvegliato!");
					}
					
				}
				catch(InterruptedException e){
					System.out.println("Eccezione nel wait");
					e.getStackTrace();
				}
				correctinput=false;
			}
		}
	}
	
	// Controlla se il carattere sia giusto e se lo � lo fa diventare un lower case
	boolean checkCorrectDecision(char dec){
		// Just to have all low cases
		if(dec=='W')
			dec='w';
		if(dec=='S')
			dec='s';
		if(dec=='A')
			dec='a';
		if(dec=='D')
			dec='d';
		if(dec=='B')
			dec='b';
		if(bombAvailable)
			return dec=='w'||dec=='s'||dec=='a'||dec=='d'||dec=='b';
		else 
			return dec=='w'||dec=='s'||dec=='a'||dec=='d';	
	}

	// Aggiunge una bomba quando il bomb manager trova un outlier
	void addABomb(int area){
		bombs.add(area);
		bombAvailable=true;
		//System.out.println("DEBUG- IOTHread.addBomb_Aggiunta nuova bomba");
	}
	
	// Rimuovo la bomba a fine della comunicazione (da GamePeer)
	void removeFirstBomb(){
		synchronized (bombs) {
			bombs.remove(0);
			if(bombs.size()>0)
				bombAvailable=true;
			else
				bombAvailable=false;
		}
	}
}
